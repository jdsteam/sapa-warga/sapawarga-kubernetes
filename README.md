# Sapawarga Kubernetes

## Start Cluster

Start single node cluster menggunakan `minikube`

```bash
minikube start
```

Aktifkan addon ingress

```bash
minikube addons enable ingress
```

Cek informasi cluster menggunakan command

```bash
kubectl cluster-info
```

## Create NGINX Ingress Controller

Buat namespace `ingress-nginx`

```bash
kubectl create namespace ingress-nginx
```

```bash
kubectl apply -f nginx-ingress-mandatory.yaml -f nginx-ingress-controller.yaml
```

Untuk deployment pada AWS EKS, jalankan

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/aws/service-l4.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/aws/patch-configmap-l4.yaml
```

Referensi: https://kubernetes.github.io/ingress-nginx/deploy/#aws

## Buat Service Account (khusus EKS)

Jalankan

```bash
kubectl apply -f eks-admin-service-account.yaml \
    -f eks-admin-cluster-role-binding.yaml
```

Disable RBAC (memiliki security drawback)

```bash
kubectl create clusterrolebinding permissive-binding \
  --clusterrole=cluster-admin \
  --user=admin \
  --user=kubelet \
  --group=system:serviceaccounts
```

## Run Services

Buat file `backend-variables.yaml` dan `webadmin-variables.yaml`, lalu ubah nilai variabel sesuai kebutuhan

```bash
cp backend-variables-sample.yaml backend-variables.yaml
nano backend-variables.yaml

cp webadmin-variables-sample.yaml webadmin-variables.yaml
nano webadmin-variables.yaml
```

Buat namespace `sapawarga`

```bash
kubectl create namespace sapawarga
```

Buat Ingress

```bash
kubectl apply -f ingress.yaml
```

Buat variables dan secrets

```bash
curl -o backend-variables.yaml https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-variables-sample.yaml

# edit variables value
nano backend-variables.yaml

kubectl apply -f backend-variables.yaml
```

```bash
curl -o webadmin-variables.yaml https://gitlab.com/jdsteam/sapa-warga/sapawarga-webadmin/raw/master/kubernetes/webadmin-variables-sample.yaml

# edit variables value
nano webadmin-variables.yaml

kubectl apply -f webadmin-variables.yaml
```

Create/jalankan services

```bash
kubectl apply \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-mysql-pv-claim.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-mysql-deployment.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-mysql-service.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-api-pv-claim.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-api-deployment.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-api-service.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-memcached-deployment.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-app/raw/master/kubernetes/backend-memcached-service.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-webadmin/raw/master/kubernetes/webadmin-deployment.yaml \
    -f https://gitlab.com/jdsteam/sapa-warga/sapawarga-webadmin/raw/master/kubernetes/webadmin-service.yaml
```

## Mengakses Service via Web

Cek IP adress dari cluster menggunakan command

```bash
kubectl cluster-info
```

Umumnya IP address dari cluster adalah 192.168.99.100. Untuk mengakses service, akses melalui URL sbb:

* API: https://192.168.99.100/api
* Admin Panel: https://192.168.99.100

## Untuk Keperluan Dev / Trobleshooting

### Melihat status internal dari suatu resource

```bash
kubectl describe [nama-resource]

# contoh: melihat status internal dari suatu pod
kubectl describe pod/sapawarga-webadmin-69dbdb7d6c-vzfdn
```

### Melihat log dari suatu resource

```bash
kubectl logs [--follow] [nama-resource]

# contoh: melihat log dari suatu pod
kubectl logs --follow pod/sapawarga-webadmin-69dbdb7d6c-vzfdn
```

### Mengakses bash suatu pod

```bash
kubectl exec -it [nama-pod] -- bash

# contoh:
kubectl exec -it pod/sapawarga-api-f98694d45-wxdvq -- bash
```

### Melihat daftar URL services via minikube

```bash
minikube service list
```

### Melihat Ingress/Load Balancer IP Address via minikube

```bash
minikube ip
```

### Resize Cluster in GKE

```bash
gcloud container clusters resize sapawarga-cluster --size=0
```

### Access to MySQL using port forwarding

Jalankan command untuk melakukan port-forwarding dari localhost:3307 ke pod database port 3306.

```bash
kubectl port-forward -n sapawarga deployment/sapawarga-mysql 3307:3306
```

Akses MySQL dengan credentials sebagai berikut:

* username: [your username]
* password: [your password]
* host: localhost
* port: 3307
